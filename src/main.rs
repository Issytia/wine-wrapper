use std::collections::{HashMap, VecDeque};
use std::env;
use std::fs::File;
use std::io::{Read, Write};
use std::os::unix::process::CommandExt;
use std::path::PathBuf;
use std::process::{exit, Command};

extern crate dirs;
extern crate shellexpand;
extern crate toml;
extern crate xdg;

use clap::Parser;
use serde_derive::Deserialize;
use shellexpand::full_with_context_no_errors;

#[derive(Debug, Deserialize)]
struct Config {
	default: String,
	wine: Option<String>,
	prefix: HashMap<String, Prefix>,
}

#[derive(Debug, Deserialize)]
struct Prefix {
	path: String,
	win32: Option<bool>,
	wine: Option<String>,
	auto: Option<Vec<String>>,
}

#[derive(Debug, Parser)]
struct Args {
	/// Prefix to use
	#[arg(short, long)]
	prefix: Option<String>,

	/// Command to run
	command: Vec<String>
}

const EXCEPTIONS: &[&str] = &["winetricks"];

fn main() {
	// read/create config
	let xdg_dirs = xdg::BaseDirectories::with_prefix("wine-wrapper").unwrap();
	let config_path = match xdg_dirs.find_config_file("config.toml") {
		None => {
			let path = xdg_dirs
				.place_config_file("config.toml")
				.expect("can't create config directory");
			let mut file = File::create(&path).expect("can't create config file");
			write!(&mut file, include_str!("default.toml")).expect("can't write default config");
			path
		}
		Some(p) => p,
	};
	let mut config_file = File::open(config_path).expect("can't open config file");
	let mut toml_str = String::new();
	config_file
		.read_to_string(&mut toml_str)
		.expect("can't read config file");

	// parse config
	let config: Config = toml::from_str(&toml_str).expect("can't parse config");
	if !config.prefix.contains_key(&config.default) {
		eprintln!("default prefix is invalid");
		exit(1);
	}
	let mut wine = String::new();

	// parse arguments
	let args = Args::parse();
	let mut prefix = match args.prefix {
		Some(s) => {
			if !config.prefix.contains_key(&s) {
				eprintln!("invalid prefix: {s}");
				exit(1);
			}
			s
		},
		None => String::new()
	};
	let mut commandline: VecDeque<String> = args.command.into();
	if commandline.len() == 0 {
		eprintln!("no command to execute");
		exit(1);
	}

	// find prefix based on executable
	let mut dir = env::current_dir().expect("no working directory?");
	let cmd = PathBuf::from(expand(&commandline[0]));
	match cmd.file_name() {
		Some(s) => match EXCEPTIONS.into_iter().position(|x| x == &s) {
			Some(_) => wine = commandline.pop_front().unwrap(),
			None => match cmd.canonicalize() {
				Ok(s) => {
					dir = s.parent().unwrap().to_path_buf();
					println!("found directory {}", dir.display());
				},
				Err(_) => ()
			}
		},
		None => {
			eprintln!("invalid command specified");
			exit(1);
		}
	}
	if prefix.is_empty() {
		'outer: for i in &config.prefix {
			if dir.starts_with(expand(&i.1.path)) {
				prefix = i.0.clone();
				println!("found prefix {}!", prefix);
			} else {
				match &i.1.auto {
					Some(v) => for j in v {
						if dir.starts_with(expand(j)) {
							prefix = i.0.clone();
							println!("found prefix {}!", prefix);
							break 'outer;
						}
					}
					None => ()
				}
			}
		}
		if prefix.is_empty() {
			prefix = config.default.clone();
			println!("using default prefix");
		}
	}

	// set up command and environment
	let prefix_data = &config.prefix[&prefix];
	if wine.is_empty() {
		wine = match &prefix_data.wine {
			Some(s) => expand(s),
			None => 	match &config.wine {
				Some(s) => expand(s),
				None => String::from("wine")
			}
		};
	}
	env::set_var("WINEPREFIX", expand(&prefix_data.path));
	match &prefix_data.win32 {
		None | Some(false) => env::set_var("WINEARCH", "win64"),
		Some(true) => env::set_var("WINEARCH", "win32"),
	}

	let mut command = Command::new(wine);
	command
		.current_dir(dir)
		.args(commandline);

	println!("command: {:?}", command);
	eprintln!("failed to launch wine: {}", command.exec());
}

fn expand(input: &str) -> String {
	full_with_context_no_errors(input, || dirs::home_dir().map(|p| p.display().to_string()),
		|s| Some(match env::var(s) {
			Ok(r) => r,
			Err(_) => format!("${s}")
		})
	).to_string()
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn expand_tilde() {
		env::set_var("HOME", "/test");

		assert_eq!(expand("~/test"), "/test/test");
	}

	#[test]
	fn expand_var() {
		env::set_var("TEST", "test");

		assert_eq!(expand("$TEST/test"), "test/test");
	}

	#[test]
	fn expand_unset() {
		env::remove_var("TEST");

		assert_eq!(expand("$TEST/test"), "$TEST/test");
	}

	#[test]
	fn expand_escaped() {
		env::set_var("TEST", "test");

		assert_eq!(expand("\\$TEST/test"), "\\$TEST/test");
	}
}
